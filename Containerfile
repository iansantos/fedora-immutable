ARG FEDORA_SPIN
ARG FEDORA_VERSION

FROM quay.io/fedora-ostree-desktops/$FEDORA_SPIN:$FEDORA_VERSION

ARG FEDORA_VERSION

ARG RPMFUSION_FREE="https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$FEDORA_VERSION.noarch.rpm"
ARG RPMFUSION_NON_FREE="https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$FEDORA_VERSION.noarch.rpm"

COPY --chmod=644 /usr /usr
COPY --chmod=644 /usr/etc/yum.repos.d/tailscale.repo /etc/yum.repos.d/tailscale.repo

RUN rpm-ostree install $RPMFUSION_FREE $RPMFUSION_NON_FREE \
    && sed -i '0,/enabled=0/s//enabled=1/' /etc/yum.repos.d/rpmfusion-free-updates-testing.repo \
    && ostree container commit

RUN rpm-ostree override remove \
    fedora-bookmarks \
    fedora-chromium-config \
    fedora-chromium-config-gnome \
    fedora-flathub-remote \
    fedora-repos-archive \
    fedora-third-party \
    fedora-workstation-repositories \
    ffmpeg-free \
    firefox \
    firefox-langpacks \
    gnome-classic-session \
    gnome-shell-extension-apps-menu \
    gnome-shell-extension-background-logo \
    gnome-shell-extension-common \
    gnome-shell-extension-launch-new-instance \
    gnome-shell-extension-places-menu \
    gnome-shell-extension-window-list \
    gnome-system-monitor \
    gnome-tour \
    libavcodec-free \
    libavdevice-free \
    libavfilter-free \
    libavformat-free \
    libavutil-free \
    libpostproc-free \
    libswresample-free \
    libswscale-free \
    mesa-va-drivers \
    yelp \
    && ostree container commit

RUN rpm-ostree install \
    android-tools \
    autorestic \
    bat \
    ddcutil \
    eza \
    fd-find \
    ffmpeg \
    ffmpeg-libs \
    ffmpegthumbnailer \
    fish \
    fzf \
    gnome-themes-extra \
    heif-pixbuf-loader \
    htop \
    intel-media-driver \
    just \
    libheif-freeworld \
    libheif-tools \
    mesa-va-drivers-freeworld.x86_64 \
    nautilus-python \
    nvtop \
    opendoas \
    openssl \
    pipewire-codec-aptx \
    podman-compose \
    rclone \
    restic \
    ripgrep \
    sd \
    tailscale \
    vim \
    yt-dlp \
    zoxide \
    zstd \
    && ostree container commit
