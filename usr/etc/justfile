[private]
default:
    @just --list --unsorted

setup-tailscale authkey:
    firewall-cmd --permanent --add-masquerade
    doas chmod 444 /etc/default/tailscaled
    doas systemctl enable --now tailscaled
    doas tailscale up --operator=$USER --ssh --advertise-exit-node --authkey {{authkey}}

install-flatpak-development:
    flatpak install -y \
        com.github.marhkb.Pods \
        com.vscodium.codium \
        io.gitlab.liferooter.TextPieces \
        me.iepure.devtoolbox \
        org.gnome.Boxes \
        org.sqlitebrowser.sqlitebrowser \
        runtime/com.visualstudio.code.tool.podman/x86_64/23.08
    flatpak update -y

install-flatpak-extra:
    flatpak install -y \
        com.github.qarmin.czkawka \
        io.gitlab.news_flash.NewsFlash \
        dev.geopjr.Collision \
        im.riot.Riot \
        io.freetubeapp.FreeTube \
        org.gnome.baobab \
        org.nickvision.money
    flatpak update -y

install-flatpak-gaming:
    flatpak install -y \
        com.heroicgameslauncher.hgl \
        com.obsproject.Studio \
        com.usebottles.bottles \
        com.valvesoftware.Steam \
        community.pathofbuilding.PathOfBuilding \
        hu.kramo.Cartridges \
        runtime/org.freedesktop.Platform.VulkanLayer.gamescope/x86_64/23.08 \
        runtime/org.freedesktop.Platform.VulkanLayer.MangoHud/x86_64/23.08 \
        runtime/org.freedesktop.Platform.VulkanLayer.OBSVkCapture/x86_64/23.08 \
        runtime/org.freedesktop.Platform.VulkanLayer.vkBasalt/x86_64/23.08
    flatpak update -y

first-boot: setup-shell setup-network setup-systemd setup-gnome setup-repositories setup-flatpak install-flatpak-base setup-gnome-extensions setup-gnome-keybindings
    -fwupdmgr update -y --no-reboot-check
    rpm-ostree cleanup -bpmr
    doas ostree admin cleanup
    doas rpm-ostree reset -lo
    reboot

next-boot:
    doas cryptsetup refresh --perf-no_read_workqueue --perf-no_write_workqueue --persistent $(df / | awk 'NR==2 {print $1}')
    doas ostree admin pin 0

setup-shell:
    doas usermod -s /bin/fish $USER

setup-network:
    doas nmcli connection modify $(nmcli -t -f NAME connection show --active | head -1) ipv4.ignore-auto-dns true ipv4.dns "9.9.9.9,149.112.112.112"
    doas nmcli connection modify $(nmcli -t -f NAME connection show --active | head -1) ipv6.ignore-auto-dns true ipv6.dns "2620:fe::fe,2620:fe::9"

setup-systemd:
    doas systemctl enable --now rpm-ostreed-automatic.timer
    doas systemctl mask --now rpm-ostree-countme.timer
    doas systemctl mask --now rpm-ostree-countme.service
    doas systemctl daemon-reload
    systemctl --user enable --now flatpak-update.timer
    systemctl --user enable --now podman-auto-update.timer
    systemctl --user enable --now podman.socket
    systemctl --user daemon-reload

setup-gnome:
    doas sd '^AutomaticLogin=.*' "AutomaticLogin=$USER" /etc/gdm/custom.conf
    gsettings set org.freedesktop.Tracker3.Miner.Files index-recursive-directories '[]'
    gsettings set org.freedesktop.Tracker3.Miner.Files index-single-directories '[]'
    gsettings set org.gnome.desktop.interface clock-format '24h'
    gsettings set org.gnome.desktop.interface clock-show-weekday true
    gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
    gsettings set org.gnome.desktop.interface enable-hot-corners false
    gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
    gsettings set org.gnome.desktop.interface show-battery-percentage true
    gsettings set org.gnome.desktop.media-handling autorun-never true
    gsettings set org.gnome.desktop.peripherals.keyboard numlock-state true
    gsettings set org.gnome.desktop.peripherals.mouse accel-profile 'flat'
    gsettings set org.gnome.desktop.peripherals.mouse speed 1.0
    gsettings set org.gnome.desktop.peripherals.touchpad speed 0.5
    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
    gsettings set org.gnome.desktop.privacy recent-files-max-age 30
    gsettings set org.gnome.desktop.privacy remove-old-temp-files true
    gsettings set org.gnome.desktop.privacy remove-old-trash-files true
    gsettings set org.gnome.desktop.privacy report-technical-problems false
    gsettings set org.gnome.desktop.search-providers disabled "['org.gnome.Nautilus.desktop']"
    gsettings set org.gnome.desktop.search-providers enabled "['hu.kramo.Cartridges.desktop']"
    gsettings set org.gnome.desktop.session idle-delay 0
    gsettings set org.gnome.desktop.sound event-sounds false
    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down '[]'
    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up '[]'
    gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,close'
    gsettings set org.gnome.mutter center-new-windows true
    gsettings set org.gnome.nautilus.compression default-compression-format 'tar.xz'
    gsettings set org.gnome.nautilus.list-view default-zoom-level 'large'
    gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'
    gsettings set org.gnome.nautilus.preferences fts-enabled false
    gsettings set org.gnome.nautilus.preferences show-create-link true
    gsettings set org.gnome.nautilus.window-state maximized true
    gsettings set org.gnome.SessionManager logout-prompt false
    gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'nothing'
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 3600
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'
    gsettings set org.gnome.shell disabled-extensions "['background-logo@fedorahosted.org']"
    gsettings set org.gnome.shell enabled-extensions "['AlphabeticalAppGrid@stuarthayhurst', 'clipboard-history@alexsaveau.dev', 'hotedge@jonathan.jdoda.ca', 'just-perfection-desktop@just-perfection', 'sp-tray@sp-tray.esenliyim.github.com', 'valent@andyholmes.ca', 'weatheroclock@CleoMenezesJr.github.io']"
    gsettings set org.gnome.shell favorite-apps "['org.gnome.Nautilus.desktop', 'app.devsuite.Ptyxis.desktop', 'org.mozilla.firefox.desktop', 'md.obsidian.Obsidian.desktop', 'org.gnome.TextEditor.desktop', 'org.signal.Signal.desktop', 'org.mozilla.Thunderbird.desktop', 'com.spotify.Client.desktop', 'com.vscodium.codium.desktop', 'com.github.marhkb.Pods.desktop', 'com.bitwarden.desktop.desktop', 'org.keepassxc.KeePassXC.desktop', 'com.github.iwalton3.jellyfin-media-player.desktop', 'com.heroicgameslauncher.hgl.desktop', 'org.gnome.Software.desktop', 'io.missioncenter.MissionCenter.desktop']"
    gsettings set org.gnome.shell last-selected-power-profile 'performance'
    gsettings set org.gnome.shell remember-mount-password true
    gsettings set org.gnome.shell.app-switcher current-workspace-only true
    gsettings set org.gnome.shell.weather locations "[<(uint32 2, <('Rio de Janeiro', 'SBRJ', true, [(-0.39968039870670141, -0.75340046626198298)], [(-0.39968039870670141, -0.75456400746111763)])>)>]"
    gsettings set org.gnome.shell.world-clocks locations "[<(uint32 2, <('Miami', 'KMIA', true, [(0.45013011811997866, -1.401785431362556)], [(0.44984580398482882, -1.3996433887715831)])>)>, <(uint32 2, <('Brussels', 'EBBR', true, [(0.88837258926511375, 0.079121586939312094)], [(0.88720903061268674, 0.07563092843532343)])>)>]"
    gsettings set org.gnome.software download-updates false
    gsettings set org.gnome.software download-updates-notify false
    gsettings set org.gnome.software show-only-free-apps true
    gsettings set org.gnome.software show-only-verified-apps false
    gsettings set org.gnome.system.locale region 'pt_BR.UTF-8'
    gsettings set org.gnome.system.location enabled false
    gsettings set org.gtk.gtk4.Settings.FileChooser show-hidden true
    gsettings set org.gtk.gtk4.Settings.FileChooser sort-directories-first true
    gsettings set org.gtk.Settings.FileChooser show-hidden true
    gsettings set org.gtk.Settings.FileChooser sort-directories-first true

setup-repositories:
    -doas rm \
        /etc/fwupd/remotes.d/lvfs-testing.conf \
        /etc/yum.repos.d/fedora-cisco-openh264.repo \
        /etc/yum.repos.d/fedora-updates-* \
        /etc/yum.repos.d/rpmfusion-*-updates-testing.repo

setup-flatpak:
    -flatpak uninstall --all --unused --delete-data -y
    -doas flatpak remote-delete fedora
    -doas flatpak remote-delete fedora-testing
    -doas flatpak remote-delete flathub
    flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak --user remote-add --if-not-exists gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
    flatpak --user install -y --from https://valent.andyholmes.ca/valent.flatpakref

install-flatpak-base:
    flatpak install -y \
        app.devsuite.Ptyxis \
        com.bitwarden.desktop \
        com.github.iwalton3.jellyfin-media-player \
        com.github.tchx84.Flatseal \
        com.mattjakeman.ExtensionManager \
        com.spotify.Client \
        io.bassi.Amberol \
        io.missioncenter.MissionCenter \
        io.mpv.Mpv \
        md.obsidian.Obsidian \
        org.gnome.Calculator \
        org.gnome.Calendar \
        org.gnome.clocks \
        org.gnome.Contacts \
        org.gnome.Loupe \
        org.gnome.NautilusPreviewer \
        org.gnome.Papers \
        org.gnome.Snapshot \
        org.gnome.TextEditor \
        org.gnome.Weather \
        org.keepassxc.KeePassXC \
        org.mozilla.firefox \
        org.mozilla.Thunderbird \
        org.onlyoffice.desktopeditors \
        org.qbittorrent.qBittorrent \
        org.signal.Signal \
        org.telegram.desktop \
        runtime/org.gtk.Gtk3theme.Adwaita-dark/x86_64/3.22
    flatpak update -y

setup-gnome-extensions: setup-dotfiles
    glib-compile-schemas $HOME/.local/share/glib-2.0/schemas
    gsettings set com.github.stunkymonkey.nautilus-open-any-terminal flatpak user
    gsettings set com.github.stunkymonkey.nautilus-open-any-terminal new-tab true
    gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal ptyxis
    gsettings set org.gnome.shell.extensions.clipboard-history cache-size 1000
    gsettings set org.gnome.shell.extensions.clipboard-history clear-history '[]'
    gsettings set org.gnome.shell.extensions.clipboard-history disable-down-arrow true
    gsettings set org.gnome.shell.extensions.clipboard-history history-size 1000
    gsettings set org.gnome.shell.extensions.clipboard-history paste-on-selection false
    gsettings set org.gnome.shell.extensions.clipboard-history private-mode false
    gsettings set org.gnome.shell.extensions.clipboard-history strip-text true
    gsettings set org.gnome.shell.extensions.clipboard-history topbar-preview-size 10
    gsettings set org.gnome.shell.extensions.hotedge fallback-in-use false
    gsettings set org.gnome.shell.extensions.hotedge suppress-activation-when-fullscreen true
    gsettings set org.gnome.shell.extensions.just-perfection activities-button false
    gsettings set org.gnome.shell.extensions.just-perfection app-menu false
    gsettings set org.gnome.shell.extensions.just-perfection app-menu-icon true
    gsettings set org.gnome.shell.extensions.just-perfection app-menu-label true
    gsettings set org.gnome.shell.extensions.just-perfection dash-icon-size 48
    gsettings set org.gnome.shell.extensions.just-perfection dash-separator false
    gsettings set org.gnome.shell.extensions.just-perfection panel true
    gsettings set org.gnome.shell.extensions.just-perfection panel-button-padding-size 3
    gsettings set org.gnome.shell.extensions.just-perfection power-icon true
    gsettings set org.gnome.shell.extensions.just-perfection window-demands-attention-focus true
    gsettings set org.gnome.shell.extensions.just-perfection workspace-switcher-should-show true
    gsettings set org.gnome.shell.extensions.just-perfection workspace-wrap-around true
    gsettings set org.gnome.shell.extensions.just-perfection world-clock false
    gsettings set org.gnome.shell.extensions.sp-tray album-max-length 100
    gsettings set org.gnome.shell.extensions.sp-tray artist-max-length 100
    gsettings set org.gnome.shell.extensions.sp-tray display-format '{track} • {artist} {pbctr}'
    gsettings set org.gnome.shell.extensions.sp-tray display-mode 0
    gsettings set org.gnome.shell.extensions.sp-tray logo-position 1
    gsettings set org.gnome.shell.extensions.sp-tray podcast-format '{track} • {album}'
    gsettings set org.gnome.shell.extensions.sp-tray position 0
    gsettings set org.gnome.shell.extensions.sp-tray title-max-length 100

setup-dotfiles:
    #!/bin/env fish
    if test $USER = ian
        git clone --depth 1 https://gitlab.com/iansantos/dotfiles.git
        cp -a dotfiles/. $HOME
        rm -rf dotfiles
        git update-index --assume-unchanged ~/.config/fish/fish_variables
    else
        git clone -b user --depth 1 https://gitlab.com/iansantos/dotfiles.git
        rm -rf dotfiles/.git
        cp -a dotfiles/. $HOME
        rm -rf dotfiles
        ./setup-dotfiles.fish
        rm setup-dotfiles.fish
    end

setup-gnome-keybindings:
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding '<Super>i'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command 'flatpak run org.mozilla.firefox -P ian'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'Firefox ian profile'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding '<Alt><Super>i'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command 'flatpak run org.mozilla.firefox -P ian --private-window'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name 'Firefox ian profile private'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ binding '<Super>d'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ command 'flatpak run org.mozilla.firefox -P dev'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ name 'Firefox dev profile'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ binding '<Alt><Super>d'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ command 'flatpak run org.mozilla.firefox -P dev --private-window'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ name 'Firefox dev profile private'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/ binding '<Super>1'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/ command 'nautilus -w'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom4/ name 'Nautilus'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/ binding '<Super>t'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/ command 'flatpak run app.devsuite.Ptyxis --tab'
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom5/ name 'Ptyxis'
    gsettings set org.gnome.shell.keybindings switch-to-application-1 '[]'

build-caddy-cloudflare-dns:
    #!/bin/env fish
    set IMAGE_NAME caddy-cloudflare-dns
    set REGISTRY_URL registry.iansantos.dev
    buildah build -t $REGISTRY_URL/$IMAGE_NAME ~/.config/containers/systemd/caddy/build/Containerfile
    if test $status -eq 0
        buildah push $REGISTRY_URL/$IMAGE_NAME
    end

build-fedora:
    #!/bin/env fish
    set FEDORA_SPIN silverblue
    set FEDORA_VERSION 40
    set REGISTRY_URL registry.iansantos.dev
    buildah build -t $REGISTRY_URL/fedora-atomic/$FEDORA_SPIN --build-arg FEDORA_VERSION=$FEDORA_VERSION --build-arg FEDORA_SPIN=$FEDORA_SPIN ~/Desktop/Projects/Gitlab/personal/fedora-atomic/Containerfile
    if test $status -eq 0
        set BUILD_VERSION $(date +%Y%m%d)
        set RELEASE_IMAGE $REGISTRY_URL/fedora-atomic/$FEDORA_SPIN
        set TAGS latest $BUILD_VERSION $FEDORA_VERSION
        for tag in $TAGS
            buildah tag $RELEASE_IMAGE $RELEASE_IMAGE:$tag
            buildah push $RELEASE_IMAGE:$tag
        end
    end

build-flutter:
    #!/bin/env fish
    set VSCODIUM_COMMIT (bat ~/.local/share/flatpak/app/com.vscodium.codium/current/active/files/share/codium/resources/app/product.json | jq -r .version | xargs -i curl -s https://api.github.com/repos/microsoft/vscode/git/ref/tags/{} | jq -r .object.sha)
    buildah build -t flutter --build-arg VSCODIUM_COMMIT=$VSCODIUM_COMMIT ~/Desktop/Projects/Gitlab/personal/android-development/Containerfile
    podman run -it -d \
        --name flutter \
        --userns keep-id \
        -v ~/Desktop/Projects/Flutter:/home/$USER/projects:Z \
        -v ~/Desktop/Projects/Releases:/home/$USER/releases:Z \
        --network host \
        --pull=newer \
        -l io.containers.autoupdate=local \
        localhost/flutter:latest

update-dev-container:
    #!/bin/env fish
    set VSCODIUM_COMMIT (bat ~/.local/share/flatpak/app/com.vscodium.codium/current/active/files/share/codium/resources/app/product.json | jq -r .version | xargs -i curl -s https://api.github.com/repos/microsoft/vscode/git/ref/tags/{} | jq -r .object.sha)
    sd '"commit": ".*"' "\"commit\": \"$VSCODIUM_COMMIT\"" ~/.local/share/flatpak/app/com.vscodium.codium/current/active/files/share/codium/resources/app/product.json
    podman start flutter
    podman exec flutter rm -r .vscodium-server/bin
    podman exec flutter curl -sOJL https://update.code.visualstudio.com/commit:$VSCODIUM_COMMIT/server-linux-x64/stable
    podman exec flutter mkdir -p .vscodium-server/bin/$VSCODIUM_COMMIT
    podman exec flutter tar --strip-components=1 -xf vscode-server-linux-x64.tar.gz -C .vscodium-server/bin/$VSCODIUM_COMMIT
    podman exec flutter rm vscode-server-linux-x64.tar.gz
    podman exec flutter sd 'if\(!.{1,2}\)\{if\(this\.[a-z]\.isBuilt\)return' 'if(false){if(this.n.isBuilt)return' .vscodium-server/bin/$VSCODIUM_COMMIT/out/vs/server/node/server.main.js
